part of coUwiki;

/// Capitalize the first letter of a string
String ucfirst(String string) {
	return string.substring(0, 1).toUpperCase() + string.substring(1);
}

/// Clear cache and reload
void hardReload() {
	cache.clear();
	context["location"].callMethod("reload", [true]);
}

/// s if plural, empty string if not
String plural(num amount) {
	if (amount == 1) {
		return "";
	} else {
		return "s";
	}
}

/// String equals, case insensitive
bool eqCi(dynamic a, dynamic b) {
	return a.toString().toLowerCase() == b.toString().toLowerCase();
}

/// Update the URL
void setHash([dynamic hash]) {
	String hashString;

	if (hash == null) {
		hashString = "";
	} else if (hash is String) {
		hashString = hash;
	} else if (hash is GameObject) {
		hashString = hash.path.toString();
	} else if (hash is ObjectPath) {
		hashString = hash.toString();
	} else {
		hashString = "";
	}

	window.location.hash = hashString;
}

/// GameObjectType to String
String typeString(GameObjectType type) {
	return type.toString().split(".")[1];
}

/// String to GameObjectType
GameObjectType stringType(String type) {
	try {
		return GameObjectType.values.singleWhere((GameObjectType t) {
			return (typeString(t).toLowerCase() == type);
		});
	} catch(_) {
		return null;
	}
}

/// Request a resource through CoU
String proxyImage(String type, String url) {
	if (type == null || url == null) {
		return null;
	}

	if (url.contains('childrenofur')) {
		return url;
	}

	return '//childrenofur.com/assets/$type/?url=$url';
}

/// Edit distance between two strings
int levenshtein(String s, String t, {bool caseSensitive: true}) {
	if (!caseSensitive) {
		s = s.toLowerCase();
		t = t.toLowerCase();
	}

	if (s == t) {
		return 0;
	}

	if (s.length == 0) {
		return t.length;
	}

	if (t.length == 0) {
		return s.length;
	}

	List<int> v0 = new List<int>.filled(t.length + 1, 0);
	List<int> v1 = new List<int>.filled(t.length + 1, 0);

	for (int i = 0; i < t.length + 1; i < i++) {
		v0[i] = i;
	}

	for (int i = 0; i < s.length; i++) {
		v1[0] = i + 1;

		for (int j = 0; j < t.length; j++) {
			int cost = (s[i] == t[j]) ? 0 : 1;
			v1[j + 1] = min(v1[j] + 1, min(v0[j + 1] + 1, v0[j] + cost));
		}

		for (int j = 0; j < t.length + 1; j++) {
			v0[j] = v1[j];
		}
	}

	return v1[t.length];
}

/// Support for decoding arrays of objects that were stored as JSON

typedef T JsonArrayDecoder<T>(Map<String, dynamic> json);

List<T> decodeJsonArray<T>(List array, JsonArrayDecoder<T> decoder) =>
	array
		.where((item) => item != null)
		.cast<Map<String, dynamic>>()
		.map((Map<String, dynamic> json) => decoder(json)).toList();

List<T> decodeJsonMap<T extends GameObject>(Map<String, dynamic> map, JsonArrayDecoder<T> decoder) {
	map.removeWhere((String key, dynamic value) => key == null || value == null);
	List<T> results = [];
	map.forEach((String key, dynamic json) => results.add(decoder(json)..id = key));
	return results;
}
