part of coUwiki;

class Item extends GameObject {
	/// Image URL of spritesheet (1, 2, 3, 4)
	String spriteUrl;

	/// About the item
	String description;

	/// Currant cost of the item, 0 for "priceless"
	int price;

	/// How many can fit in a stack
	int stacksTo;

	/// How many icons are in the spriteUrl
	int iconNum;

	/// Whether the item can hold other items
	bool isContainer;

	/// How many items it can hold
	int subSlots;

	/// Energy, Mood, Img, etc. given when consumed
	Map<String, int> consumeValues;

	Item(
		String id,
		String name,
		String category,
		String iconUrl,
		this.spriteUrl,
		this.iconNum,
		this.description,
		this.price,
		this.stacksTo,
		this.subSlots,
		this.isContainer,
		this.consumeValues
	) : super(GameObjectType.Item, id, name, category, iconUrl);

	factory Item.fromJson(Map<String, dynamic> map) {
		Map<String, int> consumeValues = {};
		map["consumeValues"].forEach((String key, dynamic value) =>
			consumeValues[key] = value as int);

		return Item(
			map["itemType"],
			map["name"],
			map["category"],
			map["iconUrl"] ?? "",
			map["spriteUrl"] ?? "",
			map["iconNum"] ?? 4,
			map["description"] ?? "",
			map["price"] ?? 0,
			map["stacksTo"] ?? 1,
			map["subSlots"] ?? 0,
			map["isContainer"] ?? false,
			consumeValues ?? {}
		);
	}

	@override
	DivElement toPage() {
		DivElement parent = super.toPage();

		parent
			..append(
				new DivElement()
					..classes = ["row", "item-header"]
					..append(
						new ImageElement(src: iconUrl)
							..classes = ["col-xs-3", "img-responsive"]
					)
					..append(
						new ParagraphElement()
							..classes = ["col-xs-9", "lead", "text-justify"]
							..text = description
					)
			)
			..append(new HRElement());

		if (price == 0) {
			parent.append(makeAlert("warning", "This item is priceless"));
		} else {
			parent.append(makeAlert("warning", "Worth around $price currant${plural(price)}"));
		}

		parent.append(makeAlert("info", "Can fit $stacksTo in a stack"));

		if (isContainer) {
			parent.append(makeAlert("success", "Can hold $subSlots item${plural(subSlots)}"));
		}

		if (consumeValues.length > 0) {
			parent
				..append(new HRElement())
				..append(new HeadingElement.h2()..text = "Consumable");

			const Map<String, String> labels = {
				"energy": "danger",
				"mood": "success",
				"img": "info"
			};

			consumeValues.forEach((String type, int amt) {
				if (amt != 0) {
					parent
						..append(
							new SpanElement()
								..classes = ["label", "label-${labels[type]}"]
								..text = (amt > 0 ? "+" : "") +
									"$amt ${type == "img" ? "iMG" : type}"
						)
						..appendText(" ");
				}
			});
		}

		return parent;
	}
}
